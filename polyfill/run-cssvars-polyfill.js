(function ($, Drupal, cssVars) {
  'use strict';

  Drupal.behaviors.cssvars_polyfill = {
    attach: function(context, settings) {
      cssVars({
        // Uncomment this option to rewrite in any browser, for debugging.
        // onlyLegacy: false
      });
    }
  };

}(jQuery, Drupal, cssVars));
