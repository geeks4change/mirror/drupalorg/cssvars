<?php

namespace Drupal\cssvars_colorng;

/**
 * Class Color
 *
 * @private
 */
class ColorNG {

  /**
   * Implements hook_config_schema_info_alter().
   */
  public static function hookConfigSchemaInfoBuild() {
    return ColorNG::configSchema();
  }

  public static function configSchema() {
    $schema = [];
    foreach (ColorInfo::all() as $themeName => $colorInfo) {
      $varDefs = [];
      foreach ($colorInfo->getUntranslatedParameterLabels() as $varName => $varLabel) {
        $varDefs[$varName] = [
          'type' => 'cssvars_color',
          'label' => $varLabel,
        ];
      }
      if ($varDefs) {
        $schema["cssvars.$themeName"] = [
          // @todo Use label.
          'label' => $colorInfo->getThemeName(),
          'type' => 'mapping',
          'mapping' => $varDefs,
        ];
      }
    }
    return $schema;
  }

  /**
   * Implements hook_library_info_alter().
   *
   * @param array $libraries
   * @param string $extension
   */
  public static function hookLibraryInfoAlter(&$libraries, $extension) {
    // Copied and adapted from @see color_library_info_alter
    $themes = array_keys(\Drupal::service('theme_handler')->listInfo());
    if (in_array($extension, $themes) && ColorInfo::get($extension)) {
      $substitutor = CssVarSubstitutor::build($extension);

      $cssPathMap = $substitutor->getCssPathMap();

      if (!empty($cssPathMap)) {
        foreach (array_keys($libraries) as $name) {
          if (isset($libraries[$name]['css'])) {
            // Override stylesheets.
            foreach ($libraries[$name]['css'] as $category => $cssAssets) {
              // Asset paths are relative to theme.
              foreach ($cssAssets as $sourcePath => $sourceMetadata) {
                // Loop over the path array with recolored CSS files to find matching
                // paths which could replace the non-recolored paths.
                foreach ($cssPathMap as $colorPath => $replacedColorPath) {
                  if ($sourcePath === $colorPath) {
                    // Replace the path to the new css file.
                    // This keeps the order of the stylesheets intact.
                    $index = array_search($sourcePath, array_keys($libraries[$name]['css'][$category]));
                    $preceding_css_assets = array_slice($libraries[$name]['css'][$category], 0, $index);
                    $succeeding_css_assets = array_slice($libraries[$name]['css'][$category], $index + 1);
                    $libraries[$name]['css'][$category] = array_merge(
                      $preceding_css_assets,
                      [$replacedColorPath => $sourceMetadata],
                      $succeeding_css_assets
                    );
                  }
                }
              }
            }
            foreach ($substitutor->getAdditionalCssPaths() as $additionalPath) {
              $libraries[$name]['css']['base'][$additionalPath] = [];
            }
          }
        }
      }
    }
  }

}
