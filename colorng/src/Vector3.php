<?php

namespace Drupal\cssvars_colorng;

class Vector3 {

  /**
   * @var int|float
   */
  private $a;

  /**
   * @var int|float
   */
  private $b;

  /**
   * @var int|float
   */
  private $c;

  /**
   * Vector3 constructor.
   *
   * @param float|int $a
   * @param float|int $b
   * @param float|int $c
   */
  public function __construct($a, $b, $c) {
    $this->a = $a;
    $this->b = $b;
    $this->c = $c;
  }

  /**
   * @return float|int
   */
  public function getA() {
    return $this->a;
  }

  /**
   * @return float|int
   */
  public function getB() {
    return $this->b;
  }

  /**
   * @return float|int
   */
  public function getC() {
    return $this->c;
  }

  /**
   * @param \Drupal\cssvars_colorng\Vector3 $x
   *
   * @return float|int
   */
  public function vectorMult(Vector3 $x) {
    return $this->a * $x->getA() + $this->b * $x->getB() + $this->c * $x->getC();
  }

  /**
   * @param int|float $k
   *
   * @return \Drupal\cssvars_colorng\Vector3
   */
  public function scalarMult($k) {
    return new static($this->a * $k, $this->b * $k, $this->c * $k);
  }

  /**
   * @param int|float $k
   *
   * @return \Drupal\cssvars_colorng\Vector3
   */
  public function scalarDiv($k) {
    return new static($this->a / $k, $this->b / $k, $this->c / $k);
  }

  /**
   * @param \Drupal\cssvars_colorng\Vector3 $x
   *
   * @return \Drupal\cssvars_colorng\Vector3
   */
  public function vectorAdd(Vector3 $x) {
    return new static($this->a + $x->getA(), $this->b + $x->getB(), $this->c + $x->getC());
  }

  /**
   * @param \Drupal\cssvars_colorng\Vector3 $x
   *
   * @return \Drupal\cssvars_colorng\Vector3
   */
  public function vectorDiff(Vector3 $x) {
    return new static($this->a - $x->getA(), $this->b - $x->getB(), $this->c - $x->getC());
  }

}
