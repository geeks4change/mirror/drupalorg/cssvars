<?php

namespace Drupal\cssvars_example;

class Controller {

  public function page() {
    $build['example'] = [
      '#markup' => '<div class="cssvars-example">' . t('This is CssVars example content.') . '</div>',
    ];
    $build['#attached']['library'][] = 'cssvars_example/style';
    return $build;
  }

}
