<?php

namespace Drupal\cssvars;

class CssVars {

  /**
   * Make var name.
   *
   * @param string $provider
   *   The provider.
   * @param string $key
   *   The key.
   *
   * @return string
   *   The var name.
   */
  public static function varName($provider, $key) {
    $provider = strtr($provider, ['_' => '-']);
    $key = strtr($key, ['_' => '-']);
    return "--$provider--$key";
  }

  /**
   * Convert key/value vars array to string.
   *
   * @param array $vars
   *
   * @return string
   */
  public static function varsToString($provider, array $vars) {
    $varLines = [];
    foreach ($vars as $key => $varValue) {
      $varName = self::varName($provider, $key);
      $varLines[] = "$varName: $varValue;";
    }
    return implode("\n", $varLines);
  }


}
