<?php

namespace Drupal\cssvars;

use Drupal\Core\Cache\CacheableMetadata;

class Hooks {

  /**
   * Implements hook_page_attachments().
   *
   * Why on earth do we put CSS to page top?
   * In a first version we used hook_page_attachments to add the build to
   * html_head. As core wants to discourage inline JS, and by analogy also
   * discourages inline CSS, there is no way to give this a weight, but the only
   * way to do it is a html_head style element.
   * See https://www.drupal.org/project/drupal/issues/2391025
   *
   * Remember that we have default variable values with the :root selector in
   * files, and overridden variable values with a more specific html:root
   * selector inline (putting them into generated files would make overrides
   * with cache contexts a nightmare, we had that with color module.)
   *
   * Now the IE polyfill recognizes both selectors but has no notion of a more
   * specific selector. It simply has later variable definitions overwrite
   * earlier ones. Which in our case is reliably the opposite of what we want.
   * Other JS basedapproaches did not work out (remove overridden variables, use
   * callbacks to sort, rewrite or filter css, use include option).
   * So moving our CSS to page top is the only viable solution here.
   *
   * @param array $pageTop
   */
  public static function hookPageTop(array &$pageTop) {
    if ($build = self::makeCssVarsBuild()) {
      $pageTop['cssvars'] = $build;
    }
  }

  /**
   * @return array
   */
  private static function makeCssVarsBuild() {
    /** @var \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager */
    $typedConfigManager = \Drupal::service('config.typed');
    /** @var \Drupal\Core\Config\ConfigFactoryInterface $configFactory */
    $configFactory = \Drupal::service('config.factory');

    $varsLines = [];
    $cacheability = new CacheableMetadata();
    $configNames = self::configNames();
    foreach ($configNames as $provider => $configName) {
      $page['#cache']['tags'][] = 'config:' . $configName;

      // First check if we have config, otherwise typed data freaks out.
      $config = $configFactory->get($configName);
      if ($config->get()) {
        $typedConfig = $typedConfigManager->get($configName);

        $vars = [];
        /** @var \Drupal\Core\TypedData\TypedDataInterface $typedCssVar */
        foreach ($typedConfig as $key => $typedCssVar) {
          // TypedConfig does not respect overrides. So use config here.
          // https://www.drupal.org/project/drupal/issues/3094810
          $varRawValue = $config->get($key);
          // Only override for non-empty values. Note that 0 is non-empty.
          if (isset($varRawValue) && $varRawValue !== '') {
            if (is_scalar($varRawValue)) {
              $varValue = self::sanitizeCssValue($varRawValue);
              $vars[$key] = $varValue;
            }
            else {
              // @todo Log this.
            }
          }
        }
        $varsLines[$provider] = CssVars::varsToString($provider, $vars);
        $cacheability = $cacheability->merge(CacheableMetadata::createFromObject($config));
      }
    }
    if ($varsLines) {
      $build = [
        '#type' => 'html_tag',
        '#tag' => 'style',
        '#value' => "html:root {\n" . implode("\n", $varsLines) . "\n}",
      ];
      $cacheability->applyTo($build);
    }
    else {
      $build = [];
    }
    return $build;
  }

  /**
   * Get relevant config names.
   *
   * @return array
   *   Config names, keyed by provider.
   */
  public static function configNames() {
    /** @var \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager */
    $typedConfigManager = \Drupal::service('config.typed');

    $definitionKeys = array_keys($typedConfigManager->getDefinitions());
    $names = [];
    foreach ($definitionKeys as $definitionKey) {
      if (preg_match('/^cssvars\.([^.]+)$/', $definitionKey, $matches)) {
        $names[$matches[1]] = $definitionKey;
      }
    }
    return $names;
  }

  /**
   * Sanitize a css value.
   *
   * Prevent from breaking out of the css context.
   *
   * @todo Put some more eyes on this.
   *
   * @param string $value
   *   The raw value.
   *
   * @return string string
   *   The sanitized value.
   */
  public static function sanitizeCssValue($value) {
    return strtr($value, [';' => '']);
  }

}
